
const User = require("./../models/User");

const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody

	return User.findOne({email: email}).then( (result, error) => {

		if(result != null){
			return `Email already exists`
		} else {
			if(result == null){
				return true
			} else {
				return error
			}
		}
	})
}

//register
module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	//save()
	return newUser.save().then( (result, error) => {
		if(result){
			return true
		} else{
			return error
		}
	})
}

//retrieve all users
module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

//login
module.exports.login = (reqBody) => {
	const {email, password} = reqBody;
	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} else {
			//what if we found the email and is existing, but the password is incorrect

			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//user profile
module.exports.getProfile = (data) => {

	const {id} = data

	return User.findById(id).then((result, err) => {

		if(result != null){
			result.password = "";
			return result
		} else {
			return false
		}
	})
}
